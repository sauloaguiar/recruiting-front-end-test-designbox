## Teste para recutramento de Front-end

### A Alboom
A Alboom é hoje a maior plataforma de serviços digitais para fotógrafos e artistas visuais da América Latina. A Alboom engloba a maior diversidade de produtos e serviços para fotógrafos, contando com sites profissionais, aprovação de fotos, aprovação de álbum, aprovação de vídeos, criação de galerias, CRM para fotógrafos, diagramação de álbuns, realidade aumentada e muito mais. Além disso temos como missão ser uma plataforma de multi aplicação que trás uma camadas gigantesca de micro-serviços que fazem com que a plataforma funcione e entregue a melhor experiência para nossos usuários.

A Alboom reconhece a importância da igualdade de oportunidades. O respeito ao ser humano, valoriza a diversidade e a convicção de que todos podem fazer algo que mudará o mundo ou a vida de alguém para melhor.

### Teste Front-end para o DesignBox, um produto Alboom!
Essa teste consiste em entendermos um pouco mais sobre seus conhecimentos com HTML, CSS, lógica, WebAPI's e WebComponents.

### Instruções

1. Crie uma conta no [gitlab.com](https://www.gitlab.com/);
2. Faça um fork e clone do projeto;
3. Crie uma branch (pode ser com seu nome mesmo);
4. Estude o problema descrito em docs/REQUISITOS.md;
5. Crie uma aplicação web seguindo as boas práticas da comunidade;
6. Implemente a solução para o problema.

**PS:** Usamos o mesmo teste para todos os níveis de front: **pleno** ou **senior**, mas procuramos adequar nossa exigência na avaliação com cada um desses níveis sem, por exemplo, exigir excelência de quem está começando.

### Esperamos que você

* Utilize algum framework (Polymer, Vue, etc.) para trabalhar com os WebComponents;
* Utilize as ultimas versões das api's da web (atender browsers evergreen);
* Utilize as ultimas versões de bibliotecas disponíveis;
* Utilize as convenções arquiteturais do framework selecionado.
* Otimize a aplicação ao máximo;
* Especifique o projeto no `README.md`;
* Documente seus componentes e funções;

### Você pode

* Utilizar um pré-processador de CSS (Scss, Stylus);
* Utilizar um task runner de sua preferência;
* Utilizar bibliotecas CSS como compass, nib, ou outras;

### Ganhe pontos extras por:

* Utilizar javascript moderno (ES6, ES7 ou ES8);
* Testes automatizados;
* Componentizar seu CSS;
* Ser fiel as especificações dos arquivos;
* Utilizar `CI` para automatização;
* UX/UI;

<br><br><sub>Os dados presentes neste teste são totalmente fictícios.</sub>
